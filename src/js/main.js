import { updateList, onSubmit } from './utilities.js';
import { ID } from './const.js';

const main = () => {
	// selection de la div avec l'id "root"
	// const root = document.getElementById(ID.ROOT);
	// const todos = generateTodo(12);
	// const todoUl = createTodoUl(todos);
	// root.append(todoUl);
	// selection de la div avec l'id "root"
	const addButton = document.getElementById(ID.ADD_BUTTON);
	addButton.addEventListener('click', onSubmit);
	updateList();
};

addEventListener('load', main);
