import { CLASSES, ID } from './const.js';

let idCounter = 1;
// récupérer la liste des todos
// const todos = [];

// GÉNÉRER LE TABLEAU
// const generateTodo = (todoNumber) => {
// 	const todos = getTodos();
// 	for (let i = 0; i < todoNumber; i++) {
// 		const todo = {
// 			id: i + 1,
// 			name: `Todo numéro ${i + 1}`,
// 			completed: false,
// 			deleted: false,
// 		};
// 		todos.push(todo);
// 	}
// 	return todos;
// };


/* const generateTodos = (nbrOfTodos) => {
  // tableau vide pour accueillir les futurs todos
  const todos = [];
  // boucle for pour la génération de todos
  for (let i = 1; i <= nbrOfTodos; i++) {
    // création d'un objet todo
    const todo = {
      id: i,
      name: `Todo numéro ${i}`,
      completed: i === 3 || i === 8,
      deleted: false,
    };
    // ajout de l'objet todo dans le tableau todos
    todos.push(todo);
  }
  // renvoi du tableau todos une fois complété
  return todos;
}; */

const createTodoCheckbox = (todo) => {
  const label = document.createElement("label");
  label.setAttribute("for", `todo-${todo.id}`);
  if (todo.completed) {
    label.classList.add(CLASSES.TODO_LABEL_COMPLETE);
  }
  label.classList.add(CLASSES.TODO_LABEL);
  label.innerText = todo.name;
  const checkbox = document.createElement("input");
  checkbox.id = `todo-${todo.id}`;
  checkbox.type = "checkbox";
  checkbox.checked = todo.completed;
  checkbox.classList.add(CLASSES.TODO_INPUT, CLASSES.SR_ONLY);
  checkbox.addEventListener("click", onCheckboxClick);
  label.append(checkbox);
  return label;
};

const switchStatus = (id, status) => {
	const todoLabel = document.querySelector(`label[for*=${id}]`);
	status
		? todoLabel.classList.add(CLASSES.TODO_LABEL_COMPLETE)
		: todoLabel.classList.remove(CLASSES.TODO_LABEL_COMPLETE);
};

const createTodoLi = () => {
  const li = document.createElement("li");
  li.classList.add(CLASSES.TODO);
  return li;
};

const createTodoUl = (todos) => {
  const ul = document.createElement("ul");
  ul.classList.add(CLASSES.TODO_LIST);
  for (let i = 0; i < todos.length; i++) {
    const todo = todos[i];
    const todoLi = createTodoLi();
    const todoCheckbox = createTodoCheckbox(todo);
    todoLi.append(todoCheckbox);
    ul.append(todoLi);
  }
  return ul;
};

// update todo 

const getTodos = () => {
	const todosStorage = localStorage.getItem('todos');
	let todos = [];
	if (todosStorage !== null) {
		todos = JSON.parse(todosStorage);
	}
	return todos;
};

const updateList = () => {
	// selection de la div avec l'id "root"
	const root = document.getElementById('root');
	const ul = document.querySelector('ul');
	if (ul !== null) {
		ul.remove();
	}
	const todos = getTodos();
	const todoUl = createTodoUl(todos);
	root.append(todoUl);
};

const saveTodos = (todos) => {
	const todosString = JSON.stringify(todos);
	localStorage.setItem('todos', todosString);
};

const createTodo = (value) => {
	const todo = {
		id: idCounter,
		name: value,
		completed: false,
		deleted: false,
	};
	return todo;
};

const getAddInputValue = () => {
	const addInput = document.getElementById('add');
	const value = addInput.value;
	return value;
};



// event

const onSubmit = (event) => {
	event.preventDefault();
	const val = getAddInputValue();
	const todo = createTodo(val);
	idCounter++;
	const todos = getTodos();
	todos.push(todo);
	saveTodos(todos);
	updateList();
};

const onCheckboxClick = (event) => {
	const checkbox = event.target;
	switchStatus(checkbox.id, checkbox.checked);
};

export { createTodoUl, updateList, onSubmit };
