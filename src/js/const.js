const CLASSES = {
	CONTAINER: 'container',
	FORM: 'form',
	ROOT: 'root',
	SR_ONLY: 'sr-only',
	TODO: 'todo',
	TODO_LABEL: 'todo__label',
	TODO_LABEL_COMPLETE: 'todo__label--completed',
	TODO_LIST: 'todos-list',
	TODO_INPUT: 'toto__input',
};

const ID = {
	ADD: 'add',
	ADD_BUTTON: 'addButton',
	ROOT: 'root',
};

export { ID, CLASSES };
